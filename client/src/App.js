import React, { Component } from 'react';
import logo from './logo.svg';

import './App.css';
import './components/Sidebar/Sidebar.css';
import './components/tools/Form/Form.css';
import Sidebar from './components/Sidebar/Sidebar';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {currentElement: null};
    //this.changeCurrentElement = this.changeCurrentElement.bind(this);
  }


  changeCurrentElement(componentToRender){
    console.log("app");
    console.log(componentToRender);
    this.setState({
        currentElement: componentToRender
    });
  }
  
  
  render() {
    return (
      <div className="App">
        <header className="App-header">          
          <h1 className="App-title">Fp Herramientas</h1>
        </header>        
        <Sidebar changeCurrentElement={this.changeCurrentElement.bind(this)} />
        {this.state.currentElement}
      </div>
    );
  }
}

export default App;
