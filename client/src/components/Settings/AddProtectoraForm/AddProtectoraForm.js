import React from 'react';
import Form from '../../tools/Form/Form';

class AddProtectoraForm extends React.Component{
    constructor(props){
        super(props);        
    }

    render(){
        return(
            <div>
                Add Protectora Form
                <Form />
            </div>);
    }

}

export default AddProtectoraForm;
