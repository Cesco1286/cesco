import React from 'react';
import SidebarElement from './SidebarElement/SidebarElement'
import SidebarElementAction from './SidebarElement/SidebarElementAction';
import AddProtectoraForm from '../Settings/AddProtectoraForm/AddProtectoraForm';

class Sidebar extends React.Component{
    constructor(props){
        super(props);
        this.changeCurrentElement = this.changeCurrentElement.bind(this);
        this.protectoraActions = [
            <SidebarElementAction name="addProtectora" onActionClick={ () => this.changeCurrentElement(<AddProtectoraForm />)} />
         ];         
        
        this.protectoraElement = <SidebarElement name="Protectora" actions={this.protectoraActions} />;
        console.log(this.protectoraElement);
    }

    changeCurrentElement(newComponent){        
        this.props.changeCurrentElement(newComponent)
    }



    render(){
        return(
            
            <div className="sidebar">                
                {this.protectoraElement}
                {this.protectoraElement}
            </div>
        );
    }

}

export default Sidebar;