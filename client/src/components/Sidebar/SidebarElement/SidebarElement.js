import React from 'react';
import SidebarElementAction from './SidebarElementAction';

class SidebarElement extends React.Component{
    constructor(props){
    //props: name, actions
        super(props);
        this.showActions = this.showActions.bind(this);    
        this.mappedActions = this.props.actions.map( (action) => {
            return <li> {action} </li>;
        });

        this.state = {displayActions: false};
    }
    
    showActions(){
        console.log("need to show/hide actions");
        this.setState({
            displayActions: !this.state.displayActions
        });
    }

    componentDidMount(){

    }

    render(){
        let actions = null;
        if(this.state.displayActions){
            actions = this.mappedActions;
        }

        return(
            <div className="sidebar-element">
                <p onClick={() => this.showActions()}>{this.props.name}</p>
                {actions}
                
            </div>
        );
    }

}

export default SidebarElement;