import React from 'react';

class SidebarElementAction extends React.Component{
    render(){
        return(
            <div className="sidebar-element-action" onClick={() => this.props.onActionClick(this.props.name)}>
                {this.props.name}
            </div>
        );
    }

}

export default SidebarElementAction;