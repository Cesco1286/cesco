import React from 'react';

class Input extends React.Component{
    //props: type
    
    render(){
        let input= null;
        switch(this.props.type){
            case "text":
            input = <input type="text" />;
            break;
            default:
            input = <input type="text" />;
            break;
        }

        return(
            <div className="fp-form-field">
                <div className="fp-form-field-input">
                    {input}
                </div>
            </div>
        );
    }

}

export default Input;