const express = require('express');
const path = require('path');

const app = express();

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.get('/api/info', (req, res) => {
    console.log("[get] /api/info");
    const info = "hola";
    res.json(info);
});

// all the request that do not match the paths above are handled here
const homePath = '/client/build/index.html';
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + homePath ));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log(`listening on port ${port}`);

